# Machine Learning For Physics

## Learning Outcome Outline					
## Pertemuan

No | Learning Outcome | Materi & Asesmen
---|---|---
8 | Training for Neural Network (Backpropagation) | [Lectures Notes/Pertemuan Online](url)
9 | Presentasi Project Medical Imaging Classification I |[Pengumpulan]()
10 |Presentasi Project Medical Imaging Classification I |[Pengumpulan]() code dan dokumen laporan(format bebas menyesuaikan) di lampirkan membuat masing-masing folder untuk setiap kelompok
11 | Recurent Neural Network| [RNN Part 1 Simple Harmonic](https://youtu.be/opGTgQMwcnE),[Multiple Couple Oscilator](https://youtu.be/6tqtXYNM3jE), [Couple Oscilator](https://youtu.be/LICwFebjFm4), [LSTM Training](https://youtu.be/qVwmtfivcD8), [code](),[Asesmen]()
12-13| ‌Boltzman Machine | [Materi Video](https://youtu.be/HU6HtpQL_YA?si=KfdEmfIUeBECvDO8),[code](https://drive.google.com/file/d/1p5v23Atpnj7KJIcgjQpIJTtILdWSIpYK/view?usp=drivesdk), [Asesmen](https://drive.google.com/drive/folders/1p5gvqRdPDAhncFCv-ocBLN8yxwnvcCaC) : Resume materi di catatan tangan 1 lembar A4 scan kumpulkan di link asesmen
14-15 | Application to Quantum Science and Technology | [Lectures Note](https://youtu.be/6JAcYnsmEAI?si=s9khpIfTo6Sq_bA_),[code](), [Asesmen]()
16| Project UAS| Tema : Quantum Device [Pengumpulan]() Project membuat video tentang ide keterbaruan Quantum Device content : pemograman/aplikasi sederhana brainstroming ide keterbaruan dan terkini dari Quantum Technology Device dibidang Fisika dan teknologi laporan dibuat dokumen riset format bebas dan video durasi 5 menit maksimal di upload di youtube tag akun @iccanotes.  
